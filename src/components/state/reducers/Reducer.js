import {
    SEARCH
} from '../actions/Action';

const initialState = [];

const showApp = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH:
        if (state.length < 10) {
            return [
                ...state, {
                    searchValue: action.payload.searchValue,
                    showData: action.payload.posts
                }
            ];
        } else {
            state.splice(0, 1);
            return [
                ...state, {
                    searchValue: action.payload.searchValue,
                    showData: action.payload.posts
                }
            ];
        }
        default:
            return state;
    }
};

export default showApp;