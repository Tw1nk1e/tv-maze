import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchShows } from '../state/actions/Action';
import PropTypes from 'prop-types';

class SearchField extends React.Component {
  static propTypes = {
    fetchShows: PropTypes.func.isRequired,
  }

  state = {
    searhValue: ''
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.fetchShows(this.state.searhValue);
    this.setState({
      searhValue: ''
    });
  }

  handleChange = (event) => {
    this.setState({
      searhValue: event.target.value
    });
  }

	render() {

		return (

      <form className="search" onSubmit={this.handleSubmit}>
          <input
            type="text"
            value={this.state.searhValue} 
            onChange={this.handleChange}
          />
          <button disabled={!this.state.searhValue} type="submit">
            SEARCH
          </button>
      </form>
		);
	}
}

	const mapDispatchToProps = dispatch => (
		bindActionCreators({ fetchShows }, dispatch)
	);

	export default connect(
    null,
		mapDispatchToProps
	)(SearchField);
		