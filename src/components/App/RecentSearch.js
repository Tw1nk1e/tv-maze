import React from 'react';
// import ReactDom from 'react-dom';
import PropTypes from 'prop-types';
import uid from 'uid';

const RecentSearch = ({ showList }) => (
	<div className="search-result">
			{showList.length !== 0 && showList.map(item => {
				return (
					<div key={uid()} className="recent-search">
						<p>{item.searchValue[0].toUpperCase() + item.searchValue.slice(1)}</p>
						<div id="test">
							{item.showData.map(item => {
								if (item.show.image === null) {
									return null;
								} else {
									return (
										<img
											key={uid()}
											src={item.show.image.medium}
											alt='pic'
										/>
									);
								}
							})
							}
						</div>
					</div>
				);
			})
			}
	</div>
);

RecentSearch.propTypes = {
	showList: PropTypes.array.isRequired,
	actions: PropTypes.object.isRequired,
};

export default RecentSearch;