import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../state/actions/Action';

import RecentSearch from './RecentSearch';

const mapStateToProps = state => ({
  showList: state.showApp
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
});

const RecentSearchCont = connect(
  mapStateToProps,
  mapDispatchToProps
)(RecentSearch);

export default RecentSearchCont;