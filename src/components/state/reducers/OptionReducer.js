import {
    NO_RESULTS,
    RESET
} from '../actions/Action';

const initialState = {
    error: false
};

const options = (state = initialState, action) => {
    let copyState = {};
    for (var key in state) {
        copyState[key] = state[key];
    }

    switch (action.type) {
        case NO_RESULTS:
            copyState.error = true;
            return copyState;
        case RESET:
            copyState.error = false;
            return copyState;
        default:
            return state;
    }
};

export default options;