import { combineReducers } from 'redux';

import showApp from './Reducer';
import options from './OptionReducer';

export const AppReducer = combineReducers({
    showApp,
    options
});