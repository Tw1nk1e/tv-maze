import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import { loadState, saveState } from '../localStorage/localStorage';
import { AppReducer } from '../reducers/AppReducer';

// CONFIGURE STORE
const LocalStorage = loadState();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(
    AppReducer,
        LocalStorage,
            composeEnhancers(applyMiddleware(thunk, promiseMiddleware()))
);

store.subscribe(() => {
    console.log('state update');
    saveState(store.getState());
  });