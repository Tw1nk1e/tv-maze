import React from 'react';
import { Provider } from 'react-redux';

import { store } from './state/stores/AppStore';

import { AppRouter } from './routers/AppRouter';

export const App = () => (
    <Provider store={store}>
        <div>
            <AppRouter />
        </div>
    </Provider>
);
