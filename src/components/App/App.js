import React from 'react';

import SearchField from './SearchField';
import ShowListCont from './ShowListCont';
// import RecentSearchCont from './RecentSearchCont';

const App = () => (
  <React.Fragment>
    <SearchField />
    {/* <RecentSearchCont /> */}
    <ShowListCont />
  </React.Fragment>
);

export default App;
