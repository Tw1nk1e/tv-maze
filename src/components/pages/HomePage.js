import React from 'react';

import App from '../App/App';

const HomePage = () => (
    <main>
        <App/>
    </main>
);

export { HomePage };
