import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../state/actions/Action';

import ShowList from './ShowList';

const mapStateToProps = state => ({
  showList: state.showApp,
  error: state.options.error
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
});

const ShowListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowList);

export default ShowListContainer;
