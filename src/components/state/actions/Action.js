import axios from 'axios';

export const SEARCH = 'SEARCH';
export const NO_RESULTS = 'NO_RESULTS';
export const RESET = 'RESET';

export const fetchShows = (searchValue) => {
  return (dispatch) => {
    return axios.get('http://api.tvmaze.com/search/shows?q=' + searchValue)
      .then(response => {
        if (response.data.length !== 0) {
          dispatch(resetError());
          dispatch(fetch(response.data, searchValue));
        } else {
          dispatch(noResults());
        }
      })
      .catch(error => {
        throw(error);
      });
  };
};

export const fetch = (posts, searchValue) => {
  return {
    type: SEARCH,
    payload: { posts, searchValue}
  };
};

export const noResults = () => {
  return {
    type: NO_RESULTS
  };
};

export const resetError = () => {
  return {
    type: RESET
  };
};