import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import uid from 'uid';

import Modal from './Modal';

class ShowList extends React.Component {

	static propTypes = {
		showList: PropTypes.array.isRequired,
		error: PropTypes.bool.isRequired,
		actions: PropTypes.object.isRequired,
	}

	static Render = null;

	state = {
		reset: true,
		error: false,
		showModal: false,
		elementId: null
	}

	handleShow = (id) => {
		this.setState({
			showModal: true,
			elementId: id
		});
		document.body.style.overflow = 'hidden';
	}

	handleHide = () => {
		this.setState({ showModal: false });
		document.body.style.overflow = 'auto';
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.error !== prevState.error) {
			return {
				reset: false,
				error: nextProps.error
			};
		} else if (nextProps.showList.length !== 0) {
			return {
				reset: false,
				error: false
			};
		}
		return null;
	}
	
	render() {
		const {showList} = this.props;
		const state = this.state;
		console.log(showList, this.state);

		if (state.error) {
			this.Render = (
				<div className="error">
					<h1>Ooops...Nothing found</h1>
				</div>
			);
		} else if (state.reset) {
			this.Render = null;
		} else {
			this.Render = (
				<div className="search-result">
					<h1>{('Search result by: ' + showList[showList.length - 1].searchValue)}</h1>
					<article className="show-collection">
						{showList[showList.length - 1].showData.map((item, index) => {
							if (item.length === 0 || item.show.image === null) {
								return null;
							} else {
								return (
									<div className="show" key={uid()} onClick={() => this.handleShow(index)}>
										<img src={item.show.image.medium} alt='pic'/>
										<div>
											<h2>{item.show.name}</h2>
											<p style={item.show.status === 'Running' ?
														{color: '#73ff73'} :
															{color: '#a10000'}}
											>
												{'Status: ' + item.show.status}
											</p>
										</div>
									</div>
								);
							}
						})
						}
					</article>
					{state.showModal &&
						ReactDOM.createPortal(
							<Modal
								data={showList[showList.length - 1].showData[state.elementId].show}
								handleHide={this.handleHide}
							/>,
							document.getElementById('portal')
						)
					}
				</div>	
			);
		}

		return (
			<div>
				{this.Render}
			</div>

		);
	}
}

export default ShowList;
		