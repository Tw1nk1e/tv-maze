import React from 'react';
import PropTypes from 'prop-types';


class Modal extends React.Component {
	static propTypes = {
		data: PropTypes.object.isRequired,
		handleHide: PropTypes.func.isRequired
	}
	
    render() {
        const show = this.props.data;
        const summary = show.summary ? show.summary.replace(/<\/?[^>]+>/g,'') : null;
        console.log(show);

		return (
			<div className='modal' onClick={this.props.handleHide}>
                <div onClick={e => e.stopPropagation()}>
                    <i onClick={this.props.handleHide} className="fas fa-times fa-3x"></i>
                    <figure>
                        <figcaption>{show.name}</figcaption>
                        <img src={show.image.medium} alt='pic'/>
                    </figure>
                    <article>
                        <h2>{show.premiered ? 'Years: ' + show.premiered.substr(0,4) : null}</h2>
                        <h2>{'Genres: ' + show.genres.join(', ')}</h2>
                        <p>{summary}</p>
                    </article>
                </div>
			</div>
		);
	}
}
  
export default Modal;