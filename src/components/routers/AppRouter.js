import React, { Fragment } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import { HomePage } from '../pages/HomePage';

export const AppRouter = () => (
    <BrowserRouter>
        <Fragment>
            <Switch>
                <Route path='/' component={HomePage} exact={true} />
                {/* <Route path='/examples' component={ExamplesPage} /> */}
                {/* <Route path='/examples' component={ExamplesPage} /> */}
                {/* <Route path='/examples' component={ExamplesPage} /> */}
                <Redirect to="/" />
            </Switch>
        </Fragment>
    </BrowserRouter>
);
